namespace :preload do
  desc "Modify action mailer's hostname in the review.rb"
  task :review do
    on roles(:app) do
      if fetch(:stage) == :review
        execute "sed -i -e \"s/review_server_name/#{fetch(:nginx_server_name)}/g\" #{current_path}/config/environments/review.rb"
      else
        puts 'Skip this task because it is not a review server.'
      end
    end
  end
end
