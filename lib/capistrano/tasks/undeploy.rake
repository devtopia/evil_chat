namespace :undeploy do
  desc 'Stop a review server when branch is deleted'
  task :start do
    on roles(:app) do
      invoke 'unicorn:stop'
      execute "sudo rm -rf #{fetch(:deploy_to)}"
      execute "sudo rm /etc/nginx/sites-enabled/#{fetch(:application)}"
    end
  end
end
