Rails.application.routes.draw do
  root to: "chat#show"

  mount ActionCable.server => '/cable'
  get  "/login", to: "auth#new"
  post "/login", to: "auth#create"
end
