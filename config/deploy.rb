# frozen_string_literal: true

lock '3.11.0'

set :repo_url, 'git@gitlab.example.com:labo/evil_chat.git'

# base
set :application,   ENV['application'] || 'evil_chat'
set :branch,        ENV['revision'] || ENV['branch_name'] || 'master'
set :rbenv_ruby,    File.read('.ruby-version').strip
set :pty,           false
set :use_sudo,      false
set :deploy_via,    :remote_cache
set :deploy_to,     "/opt/#{fetch(:application)}"
set :assets_prefix, 'packs'
set :log_level,     :debug
set :linked_dirs,   fetch(:linked_dirs, []).push(
  'log',
  'tmp/pids',
  'tmp/cache',
  'tmp/sockets',
  'vendor/bundle',
  'public/system',
  'public/uploads'
)
set :linked_files,  fetch(:linked_files, []).push(
  'config/database.yml',
  'config/credentials.yml.enc',
  'config/master.key'
)

# nvm
set :nvm_type,     :user
set :nvm_node,     'v10.15.3'
set :nvm_map_bins, %w{node npm yarn}

# redis
namespace :redis do
  %w[start stop restart].each do |command|
    desc "#{command} redis"
    task command do
      on roles(:app) do
        sudo "service redis #{command}"
      end
    end
  end
end

namespace :deploy do
  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      if fetch(:branch) == 'master'
        unless `git rev-parse HEAD` == `git rev-parse origin/master`
          puts 'WARNING: HEAD is not the same as origin/master'
          puts 'Run `git push` to sync changes.'
          exit
        end
      end
    end
  end

  desc 'upload important files'
  task :upload do
    on roles(:app) do
      sudo :mkdir, '-p', "#{shared_path}/config"
      sudo %(chown -R #{fetch(:user)}:#{fetch(:group)} /opt/#{fetch(:application)})
      sudo :mkdir, '-p', '/etc/nginx/sites-enabled'
      sudo :mkdir, '-p', '/etc/nginx/sites-available'

      upload!('config/database.yml', "#{shared_path}/config/database.yml")
      upload!('config/credentials.yml.enc', "#{shared_path}/config/credentials.yml.enc")
      upload!('config/master.key', "#{shared_path}/config/master.key")
    end
  end

  desc 'restart unicorn'
  task :restart do
    invoke 'unicorn:restart'
  end

  before :starting, :upload
  before 'check:linked_files', 'nginx:setup'
  before :starting, :check_revision
end

after 'deploy:publishing', 'deploy:restart'
after 'deploy:published', 'nginx:restart'
after 'undeploy:start', 'nginx:reload'
