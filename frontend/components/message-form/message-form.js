// client/chat.jsからsendMessageをimportする
import { sendMessage } from "client/chat";
import "./message-form.css";

const input = document.querySelector(".js-message-form--input");
const submit = document.querySelector(".js-message-form--submit");

function submitForm() {
  // sendMessageを呼び出し、その結果Rubyのsend_messageメソッドが呼ばれて
  // ActiveRecordでMessageインスタンスが作成される
  sendMessage(input.value);
  input.value = "";
  input.focus();
}

// コマンドキー（又はCtrlキー）+Enterでメッセージを送信できる
input.addEventListener("keydown", event => {
  if (event.keyCode === 13 && event.metaKey) {
    event.preventDefault();
    submitForm();
  }
});

// ボタンをクリックして送信しても良い
submit.addEventListener("click", event => {
  event.preventDefault();
  submitForm();
});
